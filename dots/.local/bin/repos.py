#!/bin/python
# Trilby's

from urllib.request import urlopen
from json import load
from time import time

with urlopen('https://www.archlinux.org/mirrors/status/json/') as f:
    mirrors = [ m for m in load(f)['urls'] if
        m['country'] == 'Poland' and
        # m['protocol'] == 'http' and
        # m['completion_pct'] == 1.0 and
        # m['delay'] <= 3600 and
        m['duration_avg'] + m['duration_stddev'] <= 1.0
    ]

for m in mirrors:
    t0 = time()
    with urlopen(m['url'] + 'core/os/x86_64/core.db') as f:
        m['rate'] = len(f.read()) / (time() - t0)

for m in sorted(mirrors, key=lambda k: k['rate'], reverse=True):
   print(f'Server = {m["url"]}$repo/os/$arch')
