#!/bin/bash

# checkupdates
repo=$(cat /tmp/pak-checkupdates | wc -l)
# auracle
aur=$(cat /tmp/pak-aurupdates | wc -l)

[[ "${repo}" -gt 0 ]] && repo="#[fg=green]${repo}#[fg=white]" || repo="${repo}"
[[ "${aur}" -gt 0 ]] && aur="#[fg=green]${aur}#[fg=white]" || aur="${aur}"

#print
echo "Repo:${repo} AUR:${aur}"
