#!/bin/bash

### Const ###
cpu_load_low=60
cpu_load_high=86

cpu_temp_low=60
cpu_temp_high=86
#############

### Cpu load ###
cpu_load=$(iostat -c 1 2 | sed '/^\s*$/d' | tail -n 1 | awk '{usage=100-$6} END {printf(usage)}')

if [[ "$cpu_load" -lt ${cpu_load_low} ]]; then 
	cpu_load="#[fg=green]${cpu_load}"
elif [[ "$cpu_load" -gt ${cpu_load_high} ]]; then
	cpu_load="#[fg=red]${cpu_load}"
else
	cpu_load="#[fg=yellow]${cpu_load}"
fi
###############

### Core 0 ###
# temp
cpu_temp0=$(sensors | grep 'Core 0' | cut -c 17-18)

if [[ "$cpu_temp0" -lt ${cpu_temp_low} ]]; then 
	cpu_temp0="#[fg=green]${cpu_temp0}"
elif [[ "$cpu_temp0" -gt ${cpu_temp_high} ]]; then
	cpu_temp0="#[fg=red]${cpu_temp0}"
else
	cpu_temp0="#[fg=yellow]${cpu_temp0}"
fi
#
##############

### Core 1 ###
# temp
cpu_temp1=$(sensors | grep 'Core 1' | cut -c 17-18)

if [[ "$cpu_temp1" -lt ${cpu_temp_low} ]]; then 
	cpu_temp1="#[fg=green]${cpu_temp1}"
elif [[ "$cpu_temp1" -gt ${cpu_temp_high} ]]; then
	cpu_temp1="#[fg=red]${cpu_temp1}"
else
	cpu_temp1="#[fg=yellow]${cpu_temp1}"
fi
#
##############

### Core 2 ###
# temp
cpu_temp2=$(sensors | grep 'Core 2' | cut -c 17-18)

if [[ "$cpu_temp2" -lt ${cpu_temp_low} ]]; then 
	cpu_temp1="#[fg=green]${cpu_temp1}"
elif [[ "$cpu_temp2" -gt ${cpu_temp_high} ]]; then
	cpu_temp2="#[fg=red]${cpu_temp2}"
else
	cpu_temp2="#[fg=yellow]${cpu_temp2}"
fi
#
##############

### Core3 ###
# temp
cpu_temp3=$(sensors | grep 'Core 3' | cut -c 17-18)

if [[ "$cpu_temp3" -lt ${cpu_temp_low} ]]; then 
	cpu_temp1="#[fg=green]${cpu_temp3}"
elif [[ "$cpu_temp3" -gt ${cpu_temp_high} ]]; then
	cpu_temp3="#[fg=red]${cpu_temp3}"
else
	cpu_temp3="#[fg=yellow]${cpu_temp3}"
fi
#
##############

### FAN ###
fan=$(sensors | grep 'fan2' | cut -c 24-31)
###########
### CPU temp ###
cpu_temp=$(sensors | grep "Core" | awk '{printf $3}' | cut -b 2-3)
################

### Print ####
echo "Load ${cpu_load}% #[default]Temp ${cpu_temp0}°${cpu_temp1}°${cpu_temp2}°${cpu_temp3}°#[default] ${fan}"
##############
