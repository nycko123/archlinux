#!/bin/bash

### Const ###
gpu_temp_low=60
gpu_temp_high=86
#############

# temp
# gpu_temp=$(sensors | \grep -A2 'radeon' | \grep 'temp' | cut -c 16-17)
# gpu_temp=$( echo "$( cat /sys/class/drm/card1/device/hwmon/hwmon*/temp1_input ) / 1000" | bc -l | cut -c -4)
gpu_temp=$( echo "$( cat /sys/class/drm/card1/device/hwmon/hwmon*/temp1_input ) / 1000" | bc -s)

if [[ "$gpu_temp" -lt "${gpu_temp_low}" ]]; then
	gpu_temp="#[fg=green]${gpu_temp}"
elif [[ "$gpu_temp" -gt "${gpu_temp_high}" ]]; then
	gpu_temp="#[fg=red]${gpu_temp}"
else
	gpu_temp="#[fg=yellow]${gpu_temp}"
fi
#
##############

### Print ####
echo "#[default]GPU ${gpu_temp}°#[default]"
##############
