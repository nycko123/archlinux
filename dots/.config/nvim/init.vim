" nocompatible has to be the first of all ( use the real vimpower )
set nocompatible

set shell=/usr/bin/zsh

if has('autocmd')
	filetype plugin indent on
	  au! BufEnter *
endif
if has('syntax') && !exists('g:syntax_on')
	syntax enable
endif

" XDG config {{{
"""""""""" XDG config """"""""""
silent execute '!mkdir -p ${XDG_CACHE_HOME}/nvim/undo'
set undodir=${XDG_CACHE_HOME}/nvim/undo
silent execute '!mkdir -p ${XDG_CACHE_HOME}/nvim/swap'
set directory=${XDG_CACHE_HOME}/nvim/swap
silent execute '!mkdir -p ${XDG_CACHE_HOME}/nvim/backup'
set backupdir=${XDG_CACHE_HOME}/nvim/backup
""set viminfo+='1000,n${XDG_CACHE_HOME}/vim/viminfo'
"set viminfofile=${XDG_CACHE_HOME}/vim/viminfo
""set runtimepath=${XDG_CONFIG_HOME}/vim,${XDG_CONFIG_HOME}/vim/after,${VIMRUNTIME}'
""""""""""""""""""""""""""""""""
" }}}

" Get the defaults that most users want.
" source $VIMRUNTIME/defaults.vim

" Replace all is aliased to S.
	nnoremap S :%s//g<Left><Left>

augroup RELOAD
	autocmd!
	" Close vim when Nerdtree is last window
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

	" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

	" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritepre * %s/\n\+\%$//e
augroup END

" backup rules
set backup			" enable backup files (.txt~)
set undofile		" enable persistent undo

" turn hybrid line numbers on
set number relativenumber
set nu rnu
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" syntax
syntax on			" enable syntax highlighting
" filetype
filetype on			" enable filetype detection
filetype plugin on	" enable filetype plugins
filetype indent on	" enable filetype indentation

" spliting buffers
set splitbelow
nnoremap <Leader>- :sp<CR>
set splitright
nnoremap <Leader>\ :vsp<CR>

nnoremap <Leader>t :20split <Bar> :terminal<CR>
nnoremap <Leader>vt :vert 50split <Bar> :terminal<CR>
" resize splits
nnoremap <leader>b<Down> :exe "resize " . (winheight(0) +5)<CR>
nnoremap <leader>b<Up> :exe "resize " . (winheight(0) -5)<CR>
nnoremap <leader>b<Left> :exe "vertical resize " . (winheight(0) +5)<CR>
nnoremap <leader>b<Right> :exe "vertical resize " . (winheight(0) -5)<CR>

" Allow moving to another buffer without saving the actual one
set hidden
" Open a new empty buffer
nmap <leader>b :enew<CR>
" Move to next/previous buffer
nmap <leader><Left> :bnext<CR>
nmap <leader><Right> :bprev<CR>
" Show all open buffers and their status
nmap <leader>bs :ls<CR>

" Use ESC for exiting terminal mode and allow moving to other splits
tnoremap <Esc> <C-\><C-n>

" tabstop settings
set tabstop=4		" a tab found in a file will be represented with 4 columns
set softtabstop=4	" when in insert mode <tab> is pressed move 4 columns
set shiftwidth=4	" indentation is 4 columns
set noexpandtab		" tabs are tabs, do not replace with spaces
set noai
"set autoindent
"set smartindent

set number					" show linenumbers
set ruler					" show ruler
set showcmd					" display incomplete commands
set wildmenu				" display completion matches in a status line
set cursorline
set noshowmatch				" don't highlight matching brackets, etc.
let g:loaded_matchparen=1	" don't highlight matching brackets, etc.

" set clipboard+=unnamedplus	" use system clipboard

" non-printing characters {{{
set list                   " show non-printing characters sometimes
set listchars=             " clear defaults
set listchars+=tab:›\ "     " show a small chevron for a tab
set listchars+=trail:·     " show a small interpunct for trailing whitespace
set listchars+=nbsp:␣      " show a small open box for non-breaking spaces
set listchars+=extends:›   " show a small chevron for text to the right
set listchars+=precedes:‹  " show a small chevron for text to the left
set listchars+=eol:\ "      " show nothing at the end of a line      "
" }}}

" moving around, searching and patterns {{{
" 2 moving around, searching and patterns
set nostartofline
set noautochdir
set wrapscan
set magic
set ignorecase
set smartcase
set gdefault
set incsearch
set nohlsearch
" }}}

" Folding {{{
set foldenable
"set foldlevelstart=10
set foldnestmax=10
set foldmethod=marker
" }}}

" mouse support
if has('mouse')
  set mouse=a
endif

" colorscheme
colorscheme molokai
" colorscheme colors-wal
let g:molokai_original = 1
" let g:rehash256 = 1
set background=dark
set bg=dark

" Statusline {{{
"""""""""""" Statusline """"""""""
set laststatus=2
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline=
set statusline+=%#PmenuSel#
" set statusline+=%{StatuslineGit()}
set statusline+=%{FugitiveStatusline()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\
"""""""""""""""""""""""""""""""""
" }}}

set showtabline=1

" Plugins {{{
""""""""""""" Plugins """""""""""
call plug#begin('~/.local/share/nvim/site/plugged')
Plug 'tomasr/molokai'
" Plug 'arcticicestudio/nord-vim'
" Plug 'ap/vim-css-color'
" Plug 'altercation/vim-colors-solarized'
" Plug 'tmux-plugins/vim-tmux-focus-events'
" Plug 'roxma/vim-tmux-clipboard'
"
" NERD tree will be loaded on the first invocation of NERDTreeToggle command
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle'  }
Plug 'somini/vim-autoclose'
"Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-commentary'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'ap/vim-buftabline'
"Plug 'valloric/youcompleteme'
Plug 'severin-lemaignan/vim-minimap'
" Plug 'dylanaraps/wal.vim'
" Plug 'vim-scripts/Txtfmt-The-Vim-Highlighter'
call plug#end()

" NERDTree options {{{
"let loaded_nerd_tree=1
"let NERDTreeMinimalUI=1
let g:NERDTreeDirArrowExpandable=' '
let g:NERDTreeDirArrowCollapsible=' '
let NERDTreeNaturalSort=1
let NERDTreeShowHidden=1
"let NERDTreeKeepTreeInNewTab=1
"autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
map <C-m> :NERDTreeFind<CR>
"}}}

" Startify options {{{
let g:startify_bookmarks = [
	\ '~/GIT/gitlab/pak/pak',
	\ '~/GIT/gitlab/pak/pak.conf',
	\ '~/.config/picom.conf',
	\ '~/.config/polybar/config',
	\ '~/.config/bspwm/bspwmrc',
	\ '~/.config/sxhkd/sxhkdrc',
	\ '~/.config/nvim/init.vim'
	\ ]
let g:startify_list_order = ['bookmarks', 'sessions', 'files', 'dir']
let g:startify_change_to_vcs_root = 1
let g:startify_custom_header = ""
" g:startify_change_to_dir
" startify_enable_special
" startify_lists
" startify_skiplist
" startify_update_oldfiles
" }}}

" Fugitive options {{{
" }}}

" Gitgutter options {{{
let g:gitgutter_map_keys = 0
let g:gitgutter_max_signs = 500  " default value
let g:gitgutter_enabled = 1
let g:gitgutter_highlight_linenrs = 1
let g:gitgutter_highlight_lines = 0
let g:gitgutter_signs = 1

" }}}

" NERDcommenter options {{{
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDToggleCheckAllLines = 1
"}}}

" buftabline options {{{
let g:buftabline_show = 2 " 0: never, 1: at least 2, 2: always
let g:buftabline_numbers = 1 " 0: never, 1: vimlike, 2: ordinal
let g:buftabline_indicators = 1 " indicate if modified
" colors
hi BufTabLineFill		guifg=#1B1D1E guibg=#1B1D1E
hi BufTabLineHidden		guibg=#1B1D1E guifg=#808080 gui=none
"hi BufTabLineCurrent
hi BufTabLineActive		ctermfg=255 ctermbg=232 guibg=#808080 gui=bold
"hi BufTabLineModifiedCurrent
"hi BufTabLineModifiedActive
"hi BufTabLineModifiedHidden
"}}}
"""""""""""""""""""""""""""""""""
" }}}
