# export MESA_LOADER_DRIVER_OVERRIDE=i915
export TERMINAL="kitty"
#export TERM="screen-256color"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"
export CHROOT="$HOME/chroot"
################ zsh env #################
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
export HISTFILE="${ZDOTDIR}/.zhistory"
export HISTTIMEFORMAT="[%F %T] "
##########################################

###### systemd --user services ###########
# export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus
##########################################

############## hardware ##################
export ALSA_CARD=PCH
export QEMU_AUDIO_DRV=alsa
##########################################

############### PGP/SSH ##################
# ssh-agent
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"
# gpg-agent
export GPG_TTY=$(tty)
# Refresh gpg-agent tty in case user switches into an X session
#gpg-connect-agent updatestartuptty /bye >/dev/null
# Set SSH to use gpg-agent
#unset SSH_AGENT_PID
#if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
#   export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
#fi
##########################################

########### .config files ################
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME}/android"
export WEECHAT_HOME="${XDG_CONFIG_HOME}/weechat"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"
export ELINKS_CONFDIR="${XDG_CONFIG_HOME}/elinks/"
export TMUXINATOR_CONFIG="${XDG_CONFIG_HOME}/tmuxinator/"
export TMUXP_CONFIGDIR="${XDG_CONFIG_HOME}/tmuxp"
export FZ_DATADIR="${XDG_CONFIG_HOME}/filezilla"
export PARALLEL="${XDG_CONFIG_HOME}/parallel"
export RANGER_LOAD_DEFAULT_RC=FALSE
export FFMPEG_DATADIR="${XDG_CONFIG_HOME}/ffmpeg"
export MPLAYER_HOME="${XDG_CONFIG_HOME}/mplayer"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
# pass
export PASSWORD_STORE_DIR="${XDG_CONFIG_HOME}/pass-store"
export PASSWORD_STORE_GENERATED_LENGTH=30
export TASKDATA="${XDG_CONFIG_HOME}/task"
export TASKRC="${TASKDATA}/taskrc"
export NOTMUCH_CONFIG="${XDG_CONFIG_HOME}/mutt/notmuch-config"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=${XDG_CONFIG_HOME}/java"
#
#export WGETRC="${XDG_CONFIG_HOME}/wget/wgetrc"
### lynx
export LYNX_CFG_PATH="${XDG_CONFIG_HOME}/lynx"
export LYNX_CFG="${LYNX_CFG_PATH}/lynx.rc"
export LYNX_TRACE_FILE="${LYNX_CFG_PATH}/lynx.trace"
export WWW_HOME="www.duckduckgo.com"
##########################################

###### data files $XDG_DATA_HOME #########
export LESSHISTFILE="${XDG_DATA_HOME}/lessht"
export GOPATH="${XDG_DATA_HOME}/go"
export MAILDIR="${XDG_DATA_HOME}/mail"
# export MAIL="${XDG_DATA_HOME}/mail/$USER"
export MAIL="${XDG_DATA_HOME}/mail"
export ATOM_HOME="${XDG_DATA_HOME}/atom"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export GEM_HOME="${XDG_DATA_HOME}/gem"
export SQLITE_HISTORY="${XDG_DATA_HOME}/sqlite_history"
unset MAILCHECK
##########################################

##### cache files $XDG_CACHE_HOME #############
export GEM_SPEC_CACHE="${XDG_CACHE_HOME}/gem"

##########################################

########### Default editor ###############
export EDITOR="/usr/bin/nvim"
export VISUAL="/usr/bin/nvim"
export SUDO_EDITOR="/usr/bin/nvim"
export DIFFPROG="/usr/bin/nvim -d"
export MANPAGER='/usr/bin/nvim +Man!'
#export PAGER="/usr/bin/most"
# VIM
#export VIMRUNTIME="/usr/share/vim/vim81"
#export VIMRUNTIME="/usr/share/nvim/runtime"
#export VIMINIT=":source $XDG_CONFIG_HOME"/vim/vimrc
##########################################

############# QT settings ################
export QT_QPA_PLATFORMTHEME="qt5ct"
export QTWEBENGINE_CHROMIUM_FLAGS="--blink-settings=darkModeEnabled=true,darkModeImagePolicy=2"
#export QT_STYLE_OVERRIDE="kvantum"
#export DISPLAY=:0
##########################################

# export YTFZF_EXTMENU=" rofi -dmenu -fuzzy -location 6 -yoffset -22 -monitor -4 -width 1850 -p search"
# export YTFZF_PREF="bestvideo[height<=?480]+bestaudio/best"
# export YTFZF_CUR=1
# export YTFZF_HIST=1

############ plasma settings #############
#export KDEHOME="${XDG_CONFIG_HOME}/kde"
#export KDEROOTHOME="/root/.config/kde"
#export KDEWM=kwin
#export KWIN_COMPOSE=N
#export KWIN_EXPLICIT_SYNC=0
#export KWIN_TRIPLE_BUFFER=1
#export KWIN_DIRECT_GL=1
#export KDE_NO_IPV6=1
##########################################

########### VDPAU settings ###############
export VDPAU_DRIVER="va_gl"
##########################################

########### VAAPI settings ###############
export LIBVA_DRIVER_NAME="i965"
##########################################

############# bspwm panel ################
export PANEL_FIFO="/tmp/panel-fifo"
##########################################

########### filetypes colors #############
export LS_COLORS='di=4;33;1'
#export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.7z=01;31:*.xz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.flac=01;35:*.mp3=01;35:*.mpc=01;35:*.ogg=01;35:*.wav=01;35:'
##########################################
