/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK &>/dev/null &
# until ping -c1 google.com &>/dev/null; do
# 	echo "offline: waiting 2s"
# 	sleep 2
# done
until true; do
	ping -c1 google.com &>/dev/null
	if [[ "$?" -gt 0 ]]; then
		echo "offline: waiting 2s"
		sleep 2
		return "$?"
	fi
done

[[ ! -e /tmp/pak-checkupdates ]] && /usr/bin/checkupdates > /tmp/pak-checkupdates &>/dev/null &
[[ ! -e /tmp/pak-aurupdates ]] && /usr/bin/auracle outdated > /tmp/pak-aurupdates &>/dev/null &
