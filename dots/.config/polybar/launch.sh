#!/bin/bash

# Terminate already running bar instances
killall -q polybar
pkill polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
polybar panel-top &>/dev/null &
polybar panel-bottom &>/dev/null &
polybar panel-top-vga &>/dev/null &
polybar panel-bottom-vga &>/dev/null &
# polybar panel-top-dp2 &>/dev/null &
# polybar panel-bottom-dp2 &>/dev/null &
