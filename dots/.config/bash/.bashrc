#
# ~/.bashrc
#
umask 077
#umask u=rwx,g=,o=

# default editor
export CHROOT="$HOME/chroot"
export EDITOR="micro"
export SUDO_EDITOR="micro"
#export VISUAL="micro"

# terminal 256-colors
#export TERM="screen-256color"
#export TERM="linux"

#history settings
export HISTCONTROL="ignoreboth:erasedups"
export HISTSIZE=10000
export HISTFILESIZE=20000
shopt -s histappend

export GTK_OVERLAY_SCROLLING=0
export QT_QPA_PLATFORMTHEME="qt5ct"

# vdpau settings
export VDPAU_DRIVER="nvidia"

# ~/bin folder
if [ -d "$HOME/bin" ]; then
	PATH="$HOME/bin:$PATH"
fi

# .bash_aliases
if [ -f ~/.aliases ]; then
	. ~/.aliases
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Coloured man pages
export PAGER="most -w"

#man() {
#	env LESS_TERMCAP_mb=$'\E[01;31m' \
#	LESS_TERMCAP_md=$'\E[01;38;5;74m' \
#	LESS_TERMCAP_me=$'\E[0m' \
#	LESS_TERMCAP_se=$'\E[0m' \
#	LESS_TERMCAP_so=$'\E[38;5;246m' \
#	LESS_TERMCAP_ue=$'\E[0m' \
#	LESS_TERMCAP_us=$'\E[04;38;5;146m' \
#	man "$@"
#}

#PS1='[\u@\h \W]\$ '

# PS1 settings
BLACK="\[\033[0;30m\]"
#RED="\[\033[0;31m\]"
GREEN="\[\033[0;32m\]"
#YELLOW="\[\033[38;5;11m\]"
#BLUE="\[\033[0;34m\]"
PURPLE="\[\033[0;35m\]"
CYAN="\[\033[0;36m\]"
WHITE="\[\033[0;37m\]"
BLUE="\[\033[38;5;75m\]"
YELLOW="\[\033[38;5;11m\]"
RED="\[\033[38;5;1m\]"
MOJGRAY="\[\033[38;5;7m\]"
CLEAR="\[$(tput sgr0)\]"
BOLD="\[$(tput bold)\]"
#MOJORANGE="\[\033[38;5;214m\]"

if [ $(id -u) -eq 0 ];
then
	MOJUSER='#'
	MOJKOLOR="\[\033[38;5;1m\]"
else
	MOJUSER='$'
	MOJKOLOR=${CYAN}
fi
git_prompt() {
	BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/*\(.*\)/\1/')
	if [ ! -z "${BRANCH}"  ]; then
		echo -ne "\033[38;5;11m${BRANCH}$(tput sgr0)"
		if [ ! -z "$(git status --short)"  ]; then
			echo -e " \033[38;5;1mX$(tput sgr0)"
		fi
	fi
}

# export PS1='┌ ${BOLD}${MOJKOLOR}\u${MOJGRAY}@${CYAN}\h:${GREEN} \w${CLEAR} $(git_prompt)\n└> ${YELLOW}\d \t${CLEAR}: ${MOJUSER} '
export PS1='\[\033[01;32m\]\u\[\033[01;31m\]@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$ $(git_prompt) '
#export PS1=$'%{\e[0;34m%}%n %{\e[0m%}at %{\e[0;33m%}%M %{\e[0m%}in %{\e[0;35m%}%~  %{\e[0m%}>> '

# term colors Xresources based
#if [ "$TERM" = "screen-256color" ]; then
#	_SEDCMD='s/.*\*color\([0-9]\{1,\}\).*#\([0-9a-fA-F]\{6\}\).*/\1 \2/p'
#	for i in $(sed -n  "$_SEDCMD" $HOME/.Xresources | awk '$1 < 16 {printf "\\e]P%X%s", $1 $2}'); do
#		echo -en "$i"
#	done
#	clear
#fi
