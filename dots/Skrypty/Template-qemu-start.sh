#!/usr/bin/env bash

ID="02"
NAME="archlinux"

_BASE="/mnt/vms/${ID}-${NAME}"

BIOS="/usr/share/ovmf/x64/OVMF_CODE.fd"
VARS="${_BASE}/vars.img"
DISK1="${_BASE}/sda.img"
DISK2="${_BASE}/sdb.img"
CDROM1="${_BASE}/cdrom.iso"
SHARED="${_BASE}/shared"

MEM=512
CPU=2

_VECTORS=$(echo "${CPU} * 2 + 2" | bc)
_MAC=$(printf 52:54:ac:1d:%.2x ${ID})

litter --class spirit -- qemu-system-x86_64 \
	-name "${NAME}" \
	-display none \
	-spice ipv4,addr=127.0.0.1,port=270${ID},disable-ticketing,disable-copy-paste,disable-agent-file-xfer,agent-mouse=off \
	-serial mon:telnet:127.0.0.1:280${ID},server,nowait,nodelay \
	-gdb tcp::260${ID} \
	-nodefaults \
	-machine q35,accel=kvm -cpu max \
	-smp cores=${CPU},threads=2,sockets=1 \
	-m ${MEM} \
	-drive if=pflash,format=raw,readonly,file="${BIOS}" \
	-drive if=pflash,format=raw,file="${VARS}" \
	-device virtio-rng \
	-device bochs-display \
	-device virtio-keyboard \
	-netdev bridge,id=bridge.0,br=vm0 -device virtio-net,mac=${_MAC}:01,netdev=bridge.0,mq=on,vectors=${_VECTORS} \
	-fsdev local,id="${NAME}",path="${SHARED}",security_model=mapped,writeout=immediate -device virtio-9p-pci,fsdev="${NAME}",mount_tag="shared" \
	-object iothread,id=iothread0 \
	-device virtio-scsi,id=scsi,iothread=iothread0,num_queues=${CPU},vectors=${_VECTORS} \
	-device scsi-hd,drive=hd1 -drive if=none,media=disk,id=hd1,file="${DISK1}",format=raw,aio=native,cache=none,discard=unmap,detect-zeroes=unmap \
	-device scsi-hd,drive=hd2 -drive if=none,media=disk,id=hd2,file="${DISK2}",format=raw,aio=native,cache=none,discard=unmap,detect-zeroes=unmap \
	-device scsi-cd,drive=cd1 -drive if=none,media=cdrom,id=cd1,file="${CDROM1}",format=raw,aio=native,cache=none

