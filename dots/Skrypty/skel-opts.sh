#!/bin/bash
# Options
# while getopts "hdfH" opt; do
# 	case ${opt} in
# 		h) 	printf "pomoc\n"
# 			exit;
# 			;;
# 		d) 	printf "pobierz\n"
# 			;;
# 		f) 	printf "wybierz format\n"
# 			;;
# 		H) 	printf "historia\n"
# 			;;
# 		*)
# 			printf "Option not found!\n"
# 			printf "pomoc\n"
# 			exit 2;
# 			;;
# 	esac
# done
# shift $((OPTIND-1))

parse_opt () {
	#the first arg is the option
	opt=$1
	#second arg is the optarg
	OPTARG="$2"
	case ${opt} in
		#Long options
		-)
			#if the option has a short version it calls this function with the opt as the shortopt
			case "${OPTARG}" in
		        help) parse_opt "h" ;;

				choose-from-history) parse_opt "H" ;;

				clear-history) parse_opt "x" ;;

				loop) parse_opt "l" ;;

				upload-time=*) upload_date_filter="${OPTARG#*=}" ;;
				last-hour) upload_date_filter="last-hour" ;;
				today) upload_date_filter="today" ;;
				this-week) upload_date_filter="this-week" ;;
				this-month) upload_date_filter="this-month" ;;
				this-year) upload_date_filter="this-year" ;;

				upload-sort=*) sort_by_filter="${OPTARG#*=}" ;;
				upload-date) sort_by_filter="upload-date" ;;
				view-count) sort_by_filter="view-count" ;;
				rating) sort_by_filter="rating" ;;

				filter-id=*|sp=*) sp="${OPTARG#*=}" ;;

				version)
				    printf "ytfzf: $YTFZF_VERSION\n"
				    printf "youtube-dl: $(youtube-dl --version)\n"
				    exit;
				    ;;

				*)
				    printf "Illegal option --$OPTARG\n"
				    exit 2 ;;
			esac ;;
		#Short options
		h) 	printf "pomoc\n"
			exit ;;

		H)	printf "historia\n" ;;

		x)	printf "czyść historię\n" && exit ;;

		v)	printf "ytfzf: $YTFZF_VERSION\n"
			exit;
			;;

		l)	printf "loop\n" ;;

		*)
			printf "bez opcji\n"
			exit 2 ;;
	esac
}

while getopts "LhDmdfxHarltsvn:U:-:" opt; do
    parse_opt "$opt" "$OPTARG"
done
shift $((OPTIND-1))
