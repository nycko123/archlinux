# mpv 1:0.29.1-8

### changes
```
- added to 'modified' group
- removed smbclient: filespace and printers access via SMB
- removed cdda support
- removed DVD menu support
- recompiled
```
