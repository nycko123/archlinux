# conky 1.11.5-2

### changes
```
- added to 'modified' group
- removed libnvctrl: NVIDIA's support
- removed libpulse: PulseAudio
```
