# auracle-userservice

### systemctl --user service to automatically find AUR updates using  'auracle sync'
```
- auracle.service - fetches list of available updates and exports it to '/tmp/aurupdates' file
- auracle.timer - runs auracle.service on a schedule
- auracle.hook - pacman's hook to reset '/tmp/aurupdates' file if update is being run between scheduled hours
```
