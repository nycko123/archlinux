# weechat 2.6-0

### changes
```
- added to 'modified' group
- compiled with python3
- removed aspell: spell checker
- removed guile2.0: scheme implementation
- removed enchant: library for spell checking
- compiled without: guile, enchant, aspell, html docs, php
- compiled with configuration moved to ~/.config/weechat
```
