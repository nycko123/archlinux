From 0f69a5225fc6b1008a8ee84d1161c9cc10f05ef3 Mon Sep 17 00:00:00 2001
From: Marius Bakke <marius@devup.no>
Date: Sat, 1 Aug 2020 18:02:21 +0200
Subject: [PATCH 01/13] testsuite: Add facility to skip tests.

The Makefile helpfully warns that some tests will fail when
--sysconfdir != /etc, but there are no provisions to easily disable
those.  This commit provides an escape hatch.

[ Lucas: add comment detailing the purpose of the field ]
---
 testsuite/testsuite.c | 9 +++++++++
 testsuite/testsuite.h | 2 ++
 2 files changed, 11 insertions(+)

diff --git a/testsuite/testsuite.c b/testsuite/testsuite.c
index e46f3d8..05df553 100644
--- a/testsuite/testsuite.c
+++ b/testsuite/testsuite.c
@@ -37,6 +37,7 @@
 #include "testsuite.h"

 static const char *ANSI_HIGHLIGHT_GREEN_ON = "\x1B[1;32m";
+static const char *ANSI_HIGHLIGHT_YELLOW_ON = "\x1B[1;33m";
 static const char *ANSI_HIGHLIGHT_RED_ON =  "\x1B[1;31m";
 static const char *ANSI_HIGHLIGHT_OFF = "\x1B[0m";

@@ -948,6 +949,14 @@ static inline int test_run_parent(const struct test *t, int fdout[2],
 	int err;
 	bool matchout, match_modules;

+	if (t->skip) {
+		LOG("%sSKIPPED%s: %s\n",
+			ANSI_HIGHLIGHT_YELLOW_ON, ANSI_HIGHLIGHT_OFF,
+			t->name);
+		err = EXIT_SUCCESS;
+		goto exit;
+	}
+
 	/* Close write-fds */
 	if (t->output.out != NULL)
 		close(fdout[1]);
diff --git a/testsuite/testsuite.h b/testsuite/testsuite.h
index f190249..c74b648 100644
--- a/testsuite/testsuite.h
+++ b/testsuite/testsuite.h
@@ -109,6 +109,8 @@ struct test {
 	const struct keyval *env_vars;
 	bool need_spawn;
 	bool expected_fail;
+	/* allow to skip tests that don't meet compile-time dependencies */
+	bool skip;
 	bool print_outputs;
 } __attribute__((aligned(8)));

--
2.30.1.457.gf011795891


From 2c74d2378657fd8831969f44c4e56ac38e327759 Mon Sep 17 00:00:00 2001
From: Marius Bakke <marius@devup.no>
Date: Sat, 1 Aug 2020 18:02:22 +0200
Subject: [PATCH 02/13] testsuite: Automatically skip tests that fail when
 sysconfdir != /etc.

---
 Makefile.am                | 12 +++++-------
 configure.ac               |  2 ++
 testsuite/test-blacklist.c |  3 +++
 testsuite/test-depmod.c    | 12 ++++++++++++
 testsuite/test-modprobe.c  |  6 ++++++
 5 files changed, 28 insertions(+), 7 deletions(-)

diff --git a/Makefile.am b/Makefile.am
index acde92b..b29e943 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -280,13 +280,7 @@ TESTSUITE_OVERRIDE_LIBS = \
 TESTSUITE_OVERRIDE_LIBS_LDFLAGS = \
 	avoid-version -module -shared -export-dynamic -rpath /nowhere -ldl

-check-sysconfdir:
-	$(AM_V_at)if test "$(sysconfdir)" != "/etc" -a "$(sysconfdir)" != "/etc/"; then \
-		echo "warning: Some tests will fail without --sysconfdir=/etc" >&2; \
-	fi
-.PHONY: check-sysconfdir
-
-check-am: rootfs check-sysconfdir
+check-am: rootfs


 EXTRA_DIST += \
@@ -341,6 +335,10 @@ TESTSUITE_LDADD = \
 	testsuite/libtestsuite.la libkmod/libkmod-internal.la \
 	shared/libshared.la

+if KMOD_SYSCONFDIR_NOT_ETC
+TESTSUITE_CPPFLAGS += -DKMOD_SYSCONFDIR_NOT_ETC
+endif
+
 check_LTLIBRARIES += testsuite/libtestsuite.la
 testsuite_libtestsuite_la_SOURCES = \
 	testsuite/testsuite.c testsuite/testsuite.h
diff --git a/configure.ac b/configure.ac
index 0cf2eda..222de3e 100644
--- a/configure.ac
+++ b/configure.ac
@@ -224,6 +224,8 @@ GTK_DOC_CHECK([1.14],[--flavour no-tmpl-flat])
 ], [
 AM_CONDITIONAL([ENABLE_GTK_DOC], false)])

+# Some tests are skipped when sysconfdir != /etc.
+AM_CONDITIONAL([KMOD_SYSCONFDIR_NOT_ETC], [test "x$sysconfdir" != "x/etc"])

 #####################################################################
 # Default CFLAGS and LDFLAGS
diff --git a/testsuite/test-blacklist.c b/testsuite/test-blacklist.c
index 969567d..d03eedb 100644
--- a/testsuite/test-blacklist.c
+++ b/testsuite/test-blacklist.c
@@ -95,6 +95,9 @@ fail_lookup:
 }

 DEFINE_TEST(blacklist_1,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if modules are correctly blacklisted",
 	.config = {
 		[TC_ROOTFS] = TESTSUITE_ROOTFS "test-blacklist/",
diff --git a/testsuite/test-depmod.c b/testsuite/test-depmod.c
index 47dafb4..261559c 100644
--- a/testsuite/test-depmod.c
+++ b/testsuite/test-depmod.c
@@ -42,6 +42,9 @@ static noreturn int depmod_modules_order_for_compressed(const struct test *t)
 }

 DEFINE_TEST(depmod_modules_order_for_compressed,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if depmod let aliases in right order when using compressed modules",
 	.config = {
 		[TC_UNAME_R] = MODULES_ORDER_UNAME,
@@ -121,6 +124,9 @@ static noreturn int depmod_detect_loop(const struct test *t)
 	exit(EXIT_FAILURE);
 }
 DEFINE_TEST(depmod_detect_loop,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if depmod detects module loops correctly",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -144,6 +150,9 @@ static noreturn int depmod_search_order_external_first(const struct test *t)
 	exit(EXIT_FAILURE);
 }
 DEFINE_TEST(depmod_search_order_external_first,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if depmod honor external keyword with higher priority",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -196,6 +205,9 @@ static noreturn int depmod_search_order_override(const struct test *t)
 	exit(EXIT_FAILURE);
 }
 DEFINE_TEST(depmod_search_order_override,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if depmod honor override keyword",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
diff --git a/testsuite/test-modprobe.c b/testsuite/test-modprobe.c
index f908d56..f6bed8b 100644
--- a/testsuite/test-modprobe.c
+++ b/testsuite/test-modprobe.c
@@ -83,6 +83,9 @@ static noreturn int modprobe_show_alias_to_none(const struct test *t)
 	exit(EXIT_FAILURE);
 }
 DEFINE_TEST(modprobe_show_alias_to_none,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if modprobe --show-depends doesn't explode with an alias to nothing",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -172,6 +175,9 @@ static noreturn int modprobe_softdep_loop(const struct test *t)
 	exit(EXIT_FAILURE);
 }
 DEFINE_TEST(modprobe_softdep_loop,
+#if defined(KMOD_SYSCONFDIR_NOT_ETC)
+        .skip = true,
+#endif
 	.description = "check if modprobe breaks softdep loop",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
--
2.30.1.457.gf011795891


From 7f60ad36f59cbe5f1d9751240505b916ff408b1d Mon Sep 17 00:00:00 2001
From: Marco d'Itri <md@linux.it>
Date: Thu, 7 Jan 2021 20:17:48 -0800
Subject: [PATCH 03/13] Fix "modinfo -F always shows name for built-ins"

Bug reported by Ben Hutchings <ben@decadent.org.uk>:
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=970871

	Now that the kernel provides module information for potentially
	modular code that's actually built-in, it's possible to query these
	built-ins with "modinfo -F".  However, this doesn't work quite right:

	$ modinfo -Flicense e1000e
	GPL v2
	$ modinfo -Flicense bitrev
	name:           bitrev
	GPL
---
 tools/modinfo.c | 6 +++++-
 1 file changed, 5 insertions(+), 1 deletion(-)

diff --git a/tools/modinfo.c b/tools/modinfo.c
index 0231bb0..f6a971f 100644
--- a/tools/modinfo.c
+++ b/tools/modinfo.c
@@ -178,7 +178,11 @@ static int modinfo_do(struct kmod_module *mod)
 	is_builtin = (filename == NULL);

 	if (is_builtin) {
-		printf("%-16s%s%c", "name:", kmod_module_get_name(mod), separator);
+		if (field == NULL)
+			printf("%-16s%s%c", "name:",
+			       kmod_module_get_name(mod), separator);
+		else if (field != NULL && streq(field, "name"))
+			printf("%s%c", kmod_module_get_name(mod), separator);
 		filename = "(builtin)";
 	}

--
2.30.1.457.gf011795891


From a7d74563245d3af1e16ac44797b550b27d2da2f5 Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Mon, 18 Jan 2021 18:13:25 -0800
Subject: [PATCH 05/13] depmod: fix precedence order

Configuration in /etc should have higher prio than /run.
Given how rarely configuration in /run is used with depmod, this is
likely not to cause any problems, even if it's a change in behavior.
---
 tools/depmod.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/tools/depmod.c b/tools/depmod.c
index 3f31cdf..8e1d9ec 100644
--- a/tools/depmod.c
+++ b/tools/depmod.c
@@ -51,8 +51,8 @@ static int verbose = DEFAULT_VERBOSE;
 static const char CFG_BUILTIN_KEY[] = "built-in";
 static const char CFG_EXTERNAL_KEY[] = "external";
 static const char *default_cfg_paths[] = {
-	"/run/depmod.d",
 	SYSCONFDIR "/depmod.d",
+	"/run/depmod.d",
 	"/lib/depmod.d",
 	NULL
 };
--
2.30.1.457.gf011795891


From 81cc4a49b3b94a65ec0d3551a9c5bafed92b0951 Mon Sep 17 00:00:00 2001
From: Dave Reisner <dreisner@archlinux.org>
Date: Wed, 25 Mar 2020 11:03:58 -0400
Subject: [PATCH 07/13] populate-modules: Use more bash, more quotes

We're already using associatives arrays, so there's no reason we should
be using 'test'.
---
 testsuite/populate-modules.sh | 18 +++++++++---------
 1 file changed, 9 insertions(+), 9 deletions(-)

diff --git a/testsuite/populate-modules.sh b/testsuite/populate-modules.sh
index 358e740..b0cc932 100755
--- a/testsuite/populate-modules.sh
+++ b/testsuite/populate-modules.sh
@@ -85,15 +85,15 @@ attach_pkcs7_array=(
     "test-modinfo/mod-simple-pkcs7.ko"
     )

-for k in ${!map[@]}; do
+for k in "${!map[@]}"; do
     dst=${ROOTFS}/$k
     src=${MODULE_PLAYGROUND}/${map[$k]}

-    if test "${dst: -1}" = "/"; then
-        install -d $dst
-        install -t $dst $src
+    if [[ $dst = */ ]]; then
+        install -d "$dst"
+        install -t "$dst" "$src"
     else
-        install -D $src $dst
+        install -D "$src" "$dst"
     fi
 done

@@ -101,7 +101,7 @@ done

 # gzip these modules
 for m in "${gzip_array[@]}"; do
-    gzip $ROOTFS/$m
+    gzip "$ROOTFS/$m"
 done

 # zstd-compress these modules
@@ -110,13 +110,13 @@ for m in "${zstd_array[@]}"; do
 done

 for m in "${attach_sha1_array[@]}"; do
-    cat ${MODULE_PLAYGROUND}/dummy.sha1 >> ${ROOTFS}/$m
+    cat "${MODULE_PLAYGROUND}/dummy.sha1" >>"${ROOTFS}/$m"
 done

 for m in "${attach_sha256_array[@]}"; do
-    cat ${MODULE_PLAYGROUND}/dummy.sha256 >> ${ROOTFS}/$m
+    cat "${MODULE_PLAYGROUND}/dummy.sha256" >>"${ROOTFS}/$m"
 done

 for m in "${attach_pkcs7_array[@]}"; do
-    cat ${MODULE_PLAYGROUND}/dummy.pkcs7 >> ${ROOTFS}/$m
+    cat "${MODULE_PLAYGROUND}/dummy.pkcs7" >>"${ROOTFS}/$m"
 done
--
2.30.1.457.gf011795891


From 2b186b6a1dcead22d0b980d926c9ede7651274ec Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 29 Jan 2021 18:28:38 -0800
Subject: [PATCH 08/13] testsuite: compress modules if feature is enabled
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Since the output needs to be the same, regardless if the module is
compressed, change populate-modules.sh to conditionally compress the
module if that feature is enabled.

This way we can execute the tests with any build-time configuration and
it should still pass.

Suggested-by: Michal Suchánek <msuchanek@suse.de>
Reviewed-by: Michal Suchánek <msuchanek@suse.de>
Tested-by: Michal Suchánek <msuchanek@suse.de>
Reviewed-by: Petr Vorel <pvorel@suse.cz>
---
 Makefile.am                   |  2 +-
 testsuite/populate-modules.sh | 27 ++++++++++++++++++---------
 testsuite/test-depmod.c       |  2 --
 3 files changed, 19 insertions(+), 12 deletions(-)

diff --git a/Makefile.am b/Makefile.am
index b29e943..24a586e 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -249,7 +249,7 @@ CREATE_ROOTFS = $(AM_V_GEN) ( $(RM) -rf $(ROOTFS) && mkdir -p $(dir $(ROOTFS)) &
 				find $(ROOTFS) -type d -exec chmod +w {} \; && \
 				find $(ROOTFS) -type f -name .gitignore -exec rm -f {} \; && \
 				$(top_srcdir)/testsuite/populate-modules.sh \
-					$(MODULE_PLAYGROUND) $(ROOTFS) ) && \
+					$(MODULE_PLAYGROUND) $(ROOTFS) $(top_builddir)/config.h ) && \
 				touch testsuite/stamp-rootfs

 build-module-playground:
diff --git a/testsuite/populate-modules.sh b/testsuite/populate-modules.sh
index b0cc932..ae43884 100755
--- a/testsuite/populate-modules.sh
+++ b/testsuite/populate-modules.sh
@@ -4,6 +4,12 @@ set -e

 MODULE_PLAYGROUND=$1
 ROOTFS=$2
+CONFIG_H=$3
+
+feature_enabled() {
+	local feature=$1
+	grep KMOD_FEATURES  $CONFIG_H | head -n 1 | grep -q \+$feature
+}

 declare -A map
 map=(
@@ -99,15 +105,18 @@ done

 # start poking the final rootfs...

-# gzip these modules
-for m in "${gzip_array[@]}"; do
-    gzip "$ROOTFS/$m"
-done
-
-# zstd-compress these modules
-for m in "${zstd_array[@]}"; do
-    zstd --rm $ROOTFS/$m
-done
+# compress modules with each format if feature is enabled
+if feature_enabled ZLIB; then
+	for m in "${gzip_array[@]}"; do
+	    gzip "$ROOTFS/$m"
+	done
+fi
+
+if feature_enabled ZSTD; then
+	for m in "${zstd_array[@]}"; do
+	    zstd --rm $ROOTFS/$m
+	done
+fi

 for m in "${attach_sha1_array[@]}"; do
     cat "${MODULE_PLAYGROUND}/dummy.sha1" >>"${ROOTFS}/$m"
diff --git a/testsuite/test-depmod.c b/testsuite/test-depmod.c
index 261559c..d7802d7 100644
--- a/testsuite/test-depmod.c
+++ b/testsuite/test-depmod.c
@@ -25,7 +25,6 @@

 #include "testsuite.h"

-#ifdef ENABLE_ZLIB
 #define MODULES_ORDER_UNAME "4.4.4"
 #define MODULES_ORDER_ROOTFS TESTSUITE_ROOTFS "test-depmod/modules-order-compressed"
 #define MODULES_ORDER_LIB_MODULES MODULES_ORDER_ROOTFS "/lib/modules/" MODULES_ORDER_UNAME
@@ -57,7 +56,6 @@ DEFINE_TEST(depmod_modules_order_for_compressed,
 			{ }
 		},
 	});
-#endif

 #define SEARCH_ORDER_SIMPLE_ROOTFS TESTSUITE_ROOTFS "test-depmod/search-order-simple"
 static noreturn int depmod_search_order_simple(const struct test *t)
--
2.30.1.457.gf011795891


From 789f04f0a955d446f8ecbc339ea017ee48e62fdd Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 29 Jan 2021 18:34:30 -0800
Subject: [PATCH 09/13] testsuite: also test xz compression

Reviewed-by: Petr Vorel <pvorel@suse.cz>
---
 testsuite/populate-modules.sh | 9 +++++++++
 1 file changed, 9 insertions(+)

diff --git a/testsuite/populate-modules.sh b/testsuite/populate-modules.sh
index ae43884..099f026 100755
--- a/testsuite/populate-modules.sh
+++ b/testsuite/populate-modules.sh
@@ -72,6 +72,9 @@ map=(

 gzip_array=(
     "test-depmod/modules-order-compressed/lib/modules/4.4.4/kernel/drivers/block/cciss.ko"
+    )
+
+xz_array=(
     "test-depmod/modules-order-compressed/lib/modules/4.4.4/kernel/drivers/scsi/scsi_mod.ko"
     )

@@ -112,6 +115,12 @@ if feature_enabled ZLIB; then
 	done
 fi

+if feature_enabled XZ; then
+	for m in "${xz_array[@]}"; do
+	    xz "$ROOTFS/$m"
+	done
+fi
+
 if feature_enabled ZSTD; then
 	for m in "${zstd_array[@]}"; do
 	    zstd --rm $ROOTFS/$m
--
2.30.1.457.gf011795891


From 0114a4b8182c175451dcf8afe81ae2d4a38a038d Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 12 Feb 2021 01:45:21 -0800
Subject: [PATCH 10/13] libkmod-config: revamp kcmdline parsing into a state
 machine

The handling of spaces and quotes is becoming hard to maintain. Convert
the parser into a state machine so we can check all the states. This
should make it easier to fix a corner case we have right now:
The kernel also accepts a quote before the module name instead of the
value. But this additional is left for later. This is purely an
algorithm change with no behavior change.

Tested-by: Jessica Yu <jeyu@kernel.org>
---
 libkmod/libkmod-config.c | 86 ++++++++++++++++++++++++----------------
 1 file changed, 52 insertions(+), 34 deletions(-)

diff --git a/libkmod/libkmod-config.c b/libkmod/libkmod-config.c
index 971f20b..d3cd10d 100644
--- a/libkmod/libkmod-config.c
+++ b/libkmod/libkmod-config.c
@@ -499,7 +499,14 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 	char buf[KCMD_LINE_SIZE];
 	int fd, err;
 	char *p, *modname,  *param = NULL, *value = NULL;
-	bool is_quoted = false, is_module = true;
+	bool is_quoted = false, iter = true;
+	enum state {
+		STATE_IGNORE,
+		STATE_MODNAME,
+		STATE_PARAM,
+		STATE_VALUE,
+		STATE_COMPLETE,
+	} state;

 	fd = open("/proc/cmdline", O_RDONLY|O_CLOEXEC);
 	if (fd < 0) {
@@ -516,54 +523,65 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 		return err;
 	}

-	for (p = buf, modname = buf; *p != '\0' && *p != '\n'; p++) {
-		if (*p == '"') {
+	state = STATE_MODNAME;
+	for (p = buf, modname = buf; iter; p++) {
+		switch (*p) {
+		case '"':
 			is_quoted = !is_quoted;
-
-			if (is_quoted) {
-				/* don't consider a module until closing quotes */
-				is_module = false;
-			} else if (param != NULL && value != NULL) {
+			break;
+		case '\0':
+		case '\n':
+			/* Stop iterating on new chars */
+			iter = false;
+			/* fall-through */
+		case ' ':
+			if (is_quoted && state == STATE_VALUE) {
+				/* no state change*/;
+			} else if (is_quoted) {
+				/* spaces are only allowed in the value part */
+				state = STATE_IGNORE;
+			} else if (state == STATE_VALUE || state == STATE_PARAM) {
+				*p = '\0';
+				state = STATE_COMPLETE;
+			} else {
 				/*
-				 * If we are indeed expecting a value and
-				 * closing quotes, then this can be considered
-				 * a valid option for a module
+				 * go to next option, ignoring any possible
+				 * partial match we have
 				 */
-				is_module = true;
+				modname = p + 1;
+				state = STATE_MODNAME;
 			}
-
-			continue;
-		}
-		if (is_quoted)
-			continue;
-
-		switch (*p) {
-		case ' ':
-			*p = '\0';
-			if (is_module)
-				kcmdline_parse_result(config, modname, param, value);
-			param = value = NULL;
-			modname = p + 1;
-			is_module = true;
 			break;
 		case '.':
-			if (param == NULL) {
+			if (state == STATE_MODNAME) {
 				*p = '\0';
 				param = p + 1;
+				state = STATE_PARAM;
+			} else if (state == STATE_PARAM) {
+				state = STATE_IGNORE;
 			}
 			break;
 		case '=':
-			if (param != NULL)
+			if (state == STATE_PARAM) {
+				/*
+				 * Don't set *p to '\0': the value var shadows
+				 * param
+				 */
 				value = p + 1;
-			else
-				is_module = false;
+				state = STATE_VALUE;
+			} else if (state == STATE_MODNAME) {
+				state = STATE_IGNORE;
+			}
 			break;
 		}
-	}

-	*p = '\0';
-	if (is_module)
-		kcmdline_parse_result(config, modname, param, value);
+		if (state == STATE_COMPLETE) {
+			kcmdline_parse_result(config, modname, param, value);
+			/* start over on next iteration */
+			modname = p + 1;
+			state = STATE_MODNAME;
+		}
+	}

 	return 0;
 }
--
2.30.1.457.gf011795891


From e4e197e4fa4eea5ab1492bea86234c0997d3e209 Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 12 Feb 2021 01:45:22 -0800
Subject: [PATCH 11/13] libkmod-config: re-quote option from kernel cmdline

It was reported that grub mangles the kernel cmdline. It turns

	acpi_cpufreq.dyndbg="file drivers/cpufreq/acpi-cpufreq.c +mpf"

	into

	"acpi_cpufreq.dyndbg=file drivers/cpufreq/acpi-cpufreq.c +mpf"

However, even though we could blame grub for doing that, the kernel
happily accepts and re-quotes it when the module is built-in.
So, it's better if kmod also understands it this way and does the same.

Here we basically add additional code to un-mangle it, moving the quote
in way that is acceptable to pass through init_module(). Note that the
interface [f]init_module() gives us mandates the quote to be part of the
value: the module name is not passed and the options are separated by
space.

Reported-by: Jiri Slaby <jirislaby@kernel.org>
Tested-by: Jessica Yu <jeyu@kernel.org>
Link: https://bugzilla.suse.com/show_bug.cgi?id=1181111#c10
---
 libkmod/libkmod-config.c                      | 36 ++++++++++++-
 .../module-param-kcmdline7/correct.txt        |  5 ++
 .../module-param-kcmdline7/correct.txt        |  5 ++
 .../module-param-kcmdline7/proc/cmdline       |  1 +
 .../module-param-kcmdline7/proc/cmdline       |  1 +
 .../module-param-kcmdline8/correct.txt        |  5 ++
 .../module-param-kcmdline7/correct.txt        |  5 ++
 .../module-param-kcmdline7/proc/cmdline       |  1 +
 .../module-param-kcmdline8/proc/cmdline       |  1 +
 testsuite/test-modprobe.c                     | 50 +++++++++++++++++++
 10 files changed, 109 insertions(+), 1 deletion(-)
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/correct.txt
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/correct.txt
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/proc/cmdline
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/proc/cmdline
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/correct.txt
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/correct.txt
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/proc/cmdline
 create mode 100644 testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/proc/cmdline

diff --git a/libkmod/libkmod-config.c b/libkmod/libkmod-config.c
index d3cd10d..2873f06 100644
--- a/libkmod/libkmod-config.c
+++ b/libkmod/libkmod-config.c
@@ -498,7 +498,7 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 {
 	char buf[KCMD_LINE_SIZE];
 	int fd, err;
-	char *p, *modname,  *param = NULL, *value = NULL;
+	char *p, *p_quote_start, *modname,  *param = NULL, *value = NULL;
 	bool is_quoted = false, iter = true;
 	enum state {
 		STATE_IGNORE,
@@ -524,10 +524,23 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 	}

 	state = STATE_MODNAME;
+	p_quote_start = NULL;
 	for (p = buf, modname = buf; iter; p++) {
 		switch (*p) {
 		case '"':
 			is_quoted = !is_quoted;
+
+			/*
+			 * only allow starting quote as first char when looking
+			 * for a modname: anything else is considered ill-formed
+			 */
+			if (is_quoted && state == STATE_MODNAME && p == modname) {
+				p_quote_start = p;
+				modname = p + 1;
+			} else if (state != STATE_VALUE) {
+				state = STATE_IGNORE;
+			}
+
 			break;
 		case '\0':
 		case '\n':
@@ -550,6 +563,7 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 				 */
 				modname = p + 1;
 				state = STATE_MODNAME;
+				p_quote_start = NULL;
 			}
 			break;
 		case '.':
@@ -576,10 +590,30 @@ static int kmod_config_parse_kcmdline(struct kmod_config *config)
 		}

 		if (state == STATE_COMPLETE) {
+			/*
+			 * We may need to re-quote to unmangle what the
+			 * bootloader passed. Example: grub passes the option as
+			 * "parport.dyndbg=file drivers/parport/ieee1284_ops.c +mpf"
+			 * instead of
+			 * parport.dyndbg="file drivers/parport/ieee1284_ops.c +mpf"
+			 */
+			if (p_quote_start && p_quote_start < modname) {
+				/*
+				 * p_quote_start
+				 * |
+				 * |modname  param  value
+				 * ||        |      |
+				 * vv        v      v
+				 * "parport\0dyndbg=file drivers/parport/ieee1284_ops.c +mpf" */
+				memmove(p_quote_start, modname, value - modname);
+				value--; modname--; param--;
+				*value = '"';
+			}
 			kcmdline_parse_result(config, modname, param, value);
 			/* start over on next iteration */
 			modname = p + 1;
 			state = STATE_MODNAME;
+			p_quote_start = NULL;
 		}
 	}

diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/correct.txt b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/correct.txt
new file mode 100644
index 0000000..d80da6d
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/correct.txt
@@ -0,0 +1,5 @@
+options psmouse foo
+options parport dyndbg="file drivers/parport/ieee1284_ops.c +mpf"
+
+# End of configuration files. Dumping indexes now:
+
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/correct.txt b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/correct.txt
new file mode 100644
index 0000000..d80da6d
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/correct.txt
@@ -0,0 +1,5 @@
+options psmouse foo
+options parport dyndbg="file drivers/parport/ieee1284_ops.c +mpf"
+
+# End of configuration files. Dumping indexes now:
+
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/proc/cmdline b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/proc/cmdline
new file mode 100644
index 0000000..86f9052
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/module-param-kcmdline7/proc/cmdline
@@ -0,0 +1 @@
+psmouse.foo parport.dyndbg="file drivers/parport/ieee1284_ops.c +mpf" quiet rw
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/proc/cmdline b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/proc/cmdline
new file mode 100644
index 0000000..86f9052
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline7/proc/cmdline
@@ -0,0 +1 @@
+psmouse.foo parport.dyndbg="file drivers/parport/ieee1284_ops.c +mpf" quiet rw
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/correct.txt b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/correct.txt
new file mode 100644
index 0000000..d80da6d
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/correct.txt
@@ -0,0 +1,5 @@
+options psmouse foo
+options parport dyndbg="file drivers/parport/ieee1284_ops.c +mpf"
+
+# End of configuration files. Dumping indexes now:
+
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/correct.txt b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/correct.txt
new file mode 100644
index 0000000..d80da6d
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/correct.txt
@@ -0,0 +1,5 @@
+options psmouse foo
+options parport dyndbg="file drivers/parport/ieee1284_ops.c +mpf"
+
+# End of configuration files. Dumping indexes now:
+
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/proc/cmdline b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/proc/cmdline
new file mode 100644
index 0000000..86f9052
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/module-param-kcmdline7/proc/cmdline
@@ -0,0 +1 @@
+psmouse.foo parport.dyndbg="file drivers/parport/ieee1284_ops.c +mpf" quiet rw
diff --git a/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/proc/cmdline b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/proc/cmdline
new file mode 100644
index 0000000..eab04ad
--- /dev/null
+++ b/testsuite/rootfs-pristine/test-modprobe/module-param-kcmdline8/proc/cmdline
@@ -0,0 +1 @@
+psmouse.foo "parport.dyndbg=file drivers/parport/ieee1284_ops.c +mpf" quiet rw
diff --git a/testsuite/test-modprobe.c b/testsuite/test-modprobe.c
index f6bed8b..dbc54f3 100644
--- a/testsuite/test-modprobe.c
+++ b/testsuite/test-modprobe.c
@@ -359,6 +359,56 @@ DEFINE_TEST(modprobe_param_kcmdline6,
 	);


+static noreturn int modprobe_param_kcmdline7(const struct test *t)
+{
+	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
+	const char *const args[] = {
+		progname,
+		"-c",
+		NULL,
+	};
+
+	test_spawn_prog(progname, args);
+	exit(EXIT_FAILURE);
+}
+DEFINE_TEST(modprobe_param_kcmdline7,
+	.description = "check if dots on other parts of kcmdline don't confuse our parser",
+	.config = {
+		[TC_UNAME_R] = "4.4.4",
+		[TC_ROOTFS] = TESTSUITE_ROOTFS "test-modprobe/module-param-kcmdline7",
+	},
+	.output = {
+		.out = TESTSUITE_ROOTFS "test-modprobe/module-param-kcmdline7/correct.txt",
+	},
+	.modules_loaded = "",
+	);
+
+
+static noreturn int modprobe_param_kcmdline8(const struct test *t)
+{
+	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
+	const char *const args[] = {
+		progname,
+		"-c",
+		NULL,
+	};
+
+	test_spawn_prog(progname, args);
+	exit(EXIT_FAILURE);
+}
+DEFINE_TEST(modprobe_param_kcmdline8,
+	.description = "check if dots on other parts of kcmdline don't confuse our parser",
+	.config = {
+		[TC_UNAME_R] = "4.4.4",
+		[TC_ROOTFS] = TESTSUITE_ROOTFS "test-modprobe/module-param-kcmdline8",
+	},
+	.output = {
+		.out = TESTSUITE_ROOTFS "test-modprobe/module-param-kcmdline8/correct.txt",
+	},
+	.modules_loaded = "",
+	);
+
+
 static noreturn int modprobe_force(const struct test *t)
 {
 	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
--
2.30.1.457.gf011795891


From 9a8cb0b0d6f7be54307f79a1ed2507b3b049a81f Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 12 Feb 2021 01:45:23 -0800
Subject: [PATCH 12/13] testsuite: allow to re-use single function for tests

Add a new DEFINE_TEST_WITH_FUNC() that accepts the function
alongside the test name. This will allow us to share a single function
for different tests.
---
 testsuite/testsuite.h | 6 ++++--
 1 file changed, 4 insertions(+), 2 deletions(-)

diff --git a/testsuite/testsuite.h b/testsuite/testsuite.h
index c74b648..44d1730 100644
--- a/testsuite/testsuite.h
+++ b/testsuite/testsuite.h
@@ -140,14 +140,16 @@ int test_run(const struct test *t);


 /* Test definitions */
-#define DEFINE_TEST(_name, ...) \
+#define DEFINE_TEST_WITH_FUNC(_name, _func, ...) \
 	static const struct test UNIQ(s##_name) \
 	__attribute__((used, section("kmod_tests"), aligned(8))) = { \
 		.name = #_name, \
-		.func = _name, \
+		.func = _func, \
 		## __VA_ARGS__ \
 	};

+#define DEFINE_TEST(_name, ...) DEFINE_TEST_WITH_FUNC(_name, _name, __VA_ARGS__)
+
 #define TESTSUITE_MAIN() \
 	extern struct test __start_kmod_tests[] __attribute__((weak, visibility("hidden")));	\
 	extern struct test __stop_kmod_tests[] __attribute__((weak, visibility("hidden")));	\
--
2.30.1.457.gf011795891


From e33655e9478b8be5dcfdfb5f076f3bc6bd948d4b Mon Sep 17 00:00:00 2001
From: Lucas De Marchi <lucas.demarchi@intel.com>
Date: Fri, 12 Feb 2021 01:45:24 -0800
Subject: [PATCH 13/13] test-modprobe: share single function for kcmdline tests

---
 testsuite/test-modprobe.c | 95 +++++----------------------------------
 1 file changed, 10 insertions(+), 85 deletions(-)

diff --git a/testsuite/test-modprobe.c b/testsuite/test-modprobe.c
index dbc54f3..0255f1a 100644
--- a/testsuite/test-modprobe.c
+++ b/testsuite/test-modprobe.c
@@ -213,7 +213,7 @@ DEFINE_TEST(modprobe_install_cmd_loop,
 	.modules_loaded = "mod-loop-b,mod-loop-a",
 	);

-static noreturn int modprobe_param_kcmdline(const struct test *t)
+static noreturn int modprobe_param_kcmdline_show_deps(const struct test *t)
 {
 	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
 	const char *const args[] = {
@@ -225,7 +225,7 @@ static noreturn int modprobe_param_kcmdline(const struct test *t)
 	test_spawn_prog(progname, args);
 	exit(EXIT_FAILURE);
 }
-DEFINE_TEST(modprobe_param_kcmdline,
+DEFINE_TEST(modprobe_param_kcmdline_show_deps,
 	.description = "check if params from kcmdline are passed to (f)init_module call",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -237,7 +237,7 @@ DEFINE_TEST(modprobe_param_kcmdline,
 	.modules_loaded = "",
 	);

-static noreturn int modprobe_param_kcmdline2(const struct test *t)
+static noreturn int modprobe_param_kcmdline(const struct test *t)
 {
 	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
 	const char *const args[] = {
@@ -249,7 +249,7 @@ static noreturn int modprobe_param_kcmdline2(const struct test *t)
 	test_spawn_prog(progname, args);
 	exit(EXIT_FAILURE);
 }
-DEFINE_TEST(modprobe_param_kcmdline2,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline2, modprobe_param_kcmdline,
 	.description = "check if params with no value are parsed correctly from kcmdline",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -261,19 +261,7 @@ DEFINE_TEST(modprobe_param_kcmdline2,
 	.modules_loaded = "",
 	);

-static noreturn int modprobe_param_kcmdline3(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline3,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline3, modprobe_param_kcmdline,
 	.description = "check if unrelated strings in kcmdline are correctly ignored",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -285,19 +273,7 @@ DEFINE_TEST(modprobe_param_kcmdline3,
 	.modules_loaded = "",
 	);

-static noreturn int modprobe_param_kcmdline4(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline4,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline4, modprobe_param_kcmdline,
 	.description = "check if unrelated strings in kcmdline are correctly ignored",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -309,19 +285,7 @@ DEFINE_TEST(modprobe_param_kcmdline4,
 	.modules_loaded = "",
 	);

-static noreturn int modprobe_param_kcmdline5(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline5,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline5, modprobe_param_kcmdline,
 	.description = "check if params with spaces are parsed correctly from kcmdline",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -333,20 +297,7 @@ DEFINE_TEST(modprobe_param_kcmdline5,
 	.modules_loaded = "",
 	);

-
-static noreturn int modprobe_param_kcmdline6(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline6,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline6, modprobe_param_kcmdline,
 	.description = "check if dots on other parts of kcmdline don't confuse our parser",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -358,20 +309,7 @@ DEFINE_TEST(modprobe_param_kcmdline6,
 	.modules_loaded = "",
 	);

-
-static noreturn int modprobe_param_kcmdline7(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline7,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline7, modprobe_param_kcmdline,
 	.description = "check if dots on other parts of kcmdline don't confuse our parser",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
@@ -383,20 +321,7 @@ DEFINE_TEST(modprobe_param_kcmdline7,
 	.modules_loaded = "",
 	);

-
-static noreturn int modprobe_param_kcmdline8(const struct test *t)
-{
-	const char *progname = ABS_TOP_BUILDDIR "/tools/modprobe";
-	const char *const args[] = {
-		progname,
-		"-c",
-		NULL,
-	};
-
-	test_spawn_prog(progname, args);
-	exit(EXIT_FAILURE);
-}
-DEFINE_TEST(modprobe_param_kcmdline8,
+DEFINE_TEST_WITH_FUNC(modprobe_param_kcmdline8, modprobe_param_kcmdline,
 	.description = "check if dots on other parts of kcmdline don't confuse our parser",
 	.config = {
 		[TC_UNAME_R] = "4.4.4",
--
2.30.1.457.gf011795891
