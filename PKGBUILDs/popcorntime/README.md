# popcorntime 0.3.10-18

### changes
```
- added to 'modified' group
- removed gconf
- removed i686 support
```
