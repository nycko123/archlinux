# steam 1.0.0.61-3

### changes
```
- added to 'modified' group
- removed zenity: graphical dialog boxes
- removed lsb-release: version query program
- removed lib32-libappindicator-gtk2: systray icon menu GTK2
- removed lib32-libindicator-gtk2: systray set of symbols GTK2
- removed lib32-libdbusmenu-gtk2: passing GTK2 menus over dbus
```
